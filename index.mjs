import sharp from "sharp";
import {VTFFile, VTFImageData} from "vtflib";

import {
  access,
  constants,
  mkdir,
  readdir,
  readFile,
  writeFile,
} from "node:fs/promises";
import {resolve} from "node:path";
import {argv, exit} from "node:process";

if (!argv[2] || argv[2] == "") {
  console.error("Provide a directory.");

  exit(1);
}

const targetDir = resolve(argv[2]);

try {
  await access(targetDir, constants.R_OK);
} catch {
  console.error("Directory not found.");
  exit(1);
}

try {
  await access("./out/vgui/ttt", constants.R_OK | constants.W_OK);
} catch {
  await mkdir("./out/vgui/ttt", {recursive: true});
}

const template = await sharp("icon_template.png");
const dummyVTF = new VTFFile(await readFile("./dummy.vtf"));

for (const file of await readdir(targetDir)) {
  if (!file.endsWith(".png")) continue;

  const icon = await sharp(resolve(targetDir, file));
  const iconMeta = await icon.metadata();
  const sizedIcon = await icon
    .resize(iconMeta.width, iconMeta.width, {
      fit: "contain",
      background: {r: 255, g: 255, b: 255, alpha: 0},
    })
    .resize(56, 56);

  const iconBuffer = await sizedIcon.raw().toBuffer();

  const dropShadowBuffer = await sharp({
    create: {
      width: 56,
      height: 56,
      channels: 4,
      background: {r: 0, g: 0, b: 0, alpha: 1},
    },
  })
    .raw()
    .toBuffer();

  for (let i = 0; i < dropShadowBuffer.length; i = i + 4) {
    dropShadowBuffer[i + 3] = iconBuffer[i + 3];
  }

  const dropShadow = await sharp(dropShadowBuffer, {
    raw: {
      width: 56,
      height: 56,
      channels: 4,
    },
  }).blur(4);

  const finalIcon = await template
    .clone()
    .composite([
      {input: await dropShadow.png().toBuffer(), top: 6, left: 4},
      {input: await sizedIcon.png().toBuffer(), top: 4, left: 4},
    ])
    .raw()
    .toBuffer();

  const vtfImage = new VTFImageData(64, 64, 1, "RGBA8888", finalIcon);
  dummyVTF.setImage(vtfImage);

  const name = file.replace(".png", "");

  await writeFile("./out/vgui/ttt/" + name + ".vtf", dummyVTF.toBuffer());

  await writeFile(
    "./out/vgui/ttt/" + name + ".vmt",
    `"UnlitGeneric"
{
    "$basetexture" "vgui/ttt/${name}"
    "$vertexcolor" 1
    "$vertexalpha" 1
    "$translucent" 1
}
`
  );
}
