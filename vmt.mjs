import {readdir, writeFile} from "node:fs/promises";

for (const file of await readdir("./out/vgui/ttt/")) {
  if (!file.endsWith(".vtf")) continue;

  const name = file.replace(".vtf", "");

  await writeFile(
    "./out/vgui/ttt/" + name + ".vmt",
    `"UnlitGeneric"
{
    "$basetexture" "vgui/ttt/${name}"
    "$vertexcolor" 1
    "$vertexalpha" 1
    "$translucent" 1
}
`
  );
}
